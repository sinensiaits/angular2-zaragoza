import { TestBed, inject } from '@angular/core/testing';

import { FotosGuardService } from './fotos-guard.service';

describe('FotosGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FotosGuardService]
    });
  });

  it('should be created', inject([FotosGuardService], (service: FotosGuardService) => {
    expect(service).toBeTruthy();
  }));
});
