import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {ToastrService} from '../shared/toastr.service';

@Injectable()
export class FotosGuardService implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const regex =  new RegExp("^F[A-Z][0-9]{1,10}$");
    console.log(regex.test(route.params['id']));
    if (regex.test(route.params['id'])) {
      return true;
    } else {
      this.router.navigate(['/fotos']);
      this.toastr.error('La signatura no es válida');
      return false;
    }
  }

  constructor(private toastr: ToastrService, private router: Router) { }

}
