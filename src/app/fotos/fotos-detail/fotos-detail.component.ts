import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IFoto} from '../fotos';
import {ActivatedRoute, Router} from '@angular/router';
import {FotosService} from '../fotos.service';
import {ToastrService} from '../../shared/toastr.service';

@Component({
  selector: 'app-fotos-detail',
  templateUrl: './fotos-detail.component.html',
  styleUrls: ['./fotos-detail.component.css']
})
export class FotosDetailComponent implements OnInit {

  @Input() foto: IFoto;
  @Output() clicked = new EventEmitter();
  private loadedAsChild = false;
  handleClick(year: number) {
     this.clicked.emit(year.toString());
  }
  constructor(private route: ActivatedRoute, private fotosService: FotosService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    if (this.route.snapshot.params['id']) {
      this.fotosService.getFoto(this.route.snapshot.params['id']).subscribe(
        foto =>  this.foto = foto[0],
        error => this.toastr.error(error),
        () => this.toastr.success('Datos cargados')
      );
    } else {
      this.loadedAsChild = true;
      console.log('No se ha pasado id');
    }
  }
  onBack(): void {
    this.router.navigate(['/fotos']);

  }
}
