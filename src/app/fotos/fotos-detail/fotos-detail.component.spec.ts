import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FotosDetailComponent } from './fotos-detail.component';

describe('FotosDetailComponent', () => {
  let component: FotosDetailComponent;
  let fixture: ComponentFixture<FotosDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FotosDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FotosDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
