
import {Observable, throwError as observableThrowError} from 'rxjs';
import {map, tap, catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {IFoto} from './fotos';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';





@Injectable()
export class FotosService {
  // tslint:disable-next-line:max-line-length
  // private _fotosUrl =  'http://datosabiertos.ayto-arganda.es/dataset/c090702e-d1ea-4b87-9e9a-4c6aa93074da/resource/2d28c885-9660-4fb0-b197-170ad267e209/download/fototeca.-archivo-de-la-ciudad.-1840---1939.json';
  private _fotosUrl = 'http://localhost:4200/assets/fototeca-1840-1939.json';
  constructor(private _http: HttpClient) {}
  getFotos(): Observable<IFoto[]> {
    return this._http.get<IFoto[]>(this._fotosUrl).pipe(
      tap(data => {
          console.log("Todo:" + JSON.stringify(data));
      }),
      catchError(FotosService.handleError),);
  }
  getFoto(signatura: string): Observable<IFoto[]> {
    return this._http.get<IFoto[]>(this._fotosUrl).pipe(
      map(data => (<IFoto[]>data).filter(foto => foto.SIGNATURA === signatura)));
  }
  private static handleError(error: HttpErrorResponse) {
    return observableThrowError(error.message);
  }
}
