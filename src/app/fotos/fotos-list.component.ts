import {Component, OnInit} from '@angular/core';
import { ToastrService } from '../shared/toastr.service';
import {IFoto} from './fotos';
import {FotosService} from './fotos.service';

@Component({
  templateUrl: './fotos-list.component.html',
  styleUrls: [ './fotos-list.component.css']
})
export class FotosListComponent implements OnInit {
  fotosFiltradas: IFoto[];
  fotos: IFoto[];
  errorMessage: string;
  title = 'Listado de Fotos Históricas';
  fotoPadding = 5;
  private _filterText: string;

  constructor(private toastr: ToastrService, private fotosService: FotosService) {}
  ngOnInit(): void {
    console.log('On Init');
    this.fotosService.getFotos().subscribe(fotos => {
        this.fotos = fotos,
        this.fotosFiltradas = this.fotos;
      },
      error => this.toastr.error(error),
      () => this.toastr.success('Datos cargados')
    );
    this.filterText = '';
  }
  get filterText(): string {
    return this._filterText;
  }
  set filterText(value: string) {
    this._filterText = value;
    this.fotosFiltradas = this.filterText ? this.aplicarFiltro(this.filterText): this.fotos;
  }
  handleChildClick(data) {
    this.filterText = data;

  }
  popupInfo(descripcion: string, title: string): void {

    this.toastr.info(descripcion, title);

  }
  filterFotos(event: any): void {
    this._filterText = event.target.value;
  }

  private aplicarFiltro(filtradoPor: string) : IFoto[] {
    filtradoPor = filtradoPor.toLowerCase();
    return this.fotos.filter((foto: IFoto) =>
      (
        foto.DESCRIPCION.toLowerCase().indexOf(filtradoPor) !== -1 ||
        foto.ANO.toString().toLowerCase().indexOf(filtradoPor) !== -1));

  }
}
