export interface IFoto {
  SIGNATURA: string;
  ANO: number;
  DESCRIPCION: string;
  AUTOR: string;
  PROCEDENCIA: string;
  FORMATO: string;
  TAMANO: string;
  LINK: string;
  IMAGEN: string;
}
