import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'autor'
})
export class AutorPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const autor_array: string[] = value.split(', ');
    return autor_array.reverse().join(' ');
  }

}
