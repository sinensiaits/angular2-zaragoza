import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FotosListComponent} from './fotos/fotos-list.component';
import {ToastrService} from './shared/toastr.service';
import {FormsModule} from '@angular/forms';
import { AutorPipe } from './shared/autor.pipe';
import { FotosDetailComponent } from './fotos/fotos-detail/fotos-detail.component';
import {FotosService} from './fotos/fotos.service';
import {HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import { InfoComponent } from './info/info.component';
import { FotosGuardService } from './fotos/fotos-guard.service';


@NgModule({
  declarations: [
    AppComponent, FotosListComponent, AutorPipe, FotosDetailComponent, NavbarComponent, InfoComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule, RouterModule.forRoot(appRoutes)
  ],
  providers: [ ToastrService, FotosService, FotosGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
