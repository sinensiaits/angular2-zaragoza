import { Routes } from '@angular/router';
import {FotosListComponent} from './fotos/fotos-list.component';
import {FotosDetailComponent} from './fotos/fotos-detail/fotos-detail.component';
import {InfoComponent} from './info/info.component';
import {FotosGuardService} from './fotos/fotos-guard.service';

export const appRoutes: Routes = [
  { path: '', component: FotosListComponent, pathMatch: 'full'},
  { path: 'fotos', component: FotosListComponent, pathMatch: 'full'},
  { path: 'fotos/:id', component: FotosDetailComponent, pathMatch: 'full', canActivate: [FotosGuardService]},
  { path: 'info', component: InfoComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/fotos', pathMatch: 'full' }
];
